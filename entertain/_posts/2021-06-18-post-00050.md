---
layout: post
title: "영어문장 사귀다 영어로, 넷플릭스 로코영화 '겉보기엔 멀쩡한 남자'"
toc: true
---

 넷플릭스 6월 신작 코믹영화 "겉보기엔 멀쩡한 남자"에 나오는 영어표현 들을 알아봅시다. "겉보기엔 멀쩡한 남자"는 장르가 로코영화(로맨틱 코메디)입니다. 상의물론 사귀다 영어표현이 나오는게 당연하겠죠? 남자가 여자에게 사귀자고 고백합니다. "나랑 사귀자"를 영어로 "I want you to be my girlfriend."라고 하시면 됩니다. 원체 어렵지 않죠? 일말 더 로맨틱한 표현없냐고요? 형씨 보다는 앞에 분위기를 조성하는 여러가지 말들이 더한층 좋지 않을까요? 장소도 중요하고 말입니다.
 

 

## 논제 Good on paper 뜻은?
 

 자! 각하 영화 제목부터가 편시 재미있습니다. "Good on paper"인데요. 직역하자면 "서류상으론 괜찮은"입니다. 이걸 훈민정음 제목으로 하면 이상하잖아요. 그런데 한국어 제목을 "겉보기엔 멀쩡한 남자"라고 지은 이유가 있습니다.
 

 Good on paper라고 하면 좋은 집안, 학교, 회사를 나왔지만 심각한 너드/ 덕후라든가,  콘테스트 중독자라든가, 열매 취향이 이상한 등등 현실과 슬쩍 동떨어진 삶을 사는 사람을 가리키는 말입니다.
 

 그리고 실제 예고편을 보면, 여주인공이 무지무지 멀쩡해보이는 남자의 비밀을 파헤치려고 하죠. 아울러 남자도 어찌나 수상하긴 합니다. 따라서 괜히 언문 제목을 "겉보기엔 멀쩡한 남자"라고 지은게 아닌거죠!!
 

 

### 이놈 외면 다양한 영어문장과 표현
 

 A salty 35!!
 여주의 친구가 여주인공에게 "성질 드러운 35살"이라고 합니다. 여기서 salty는 형용사로 "맛이 짠" 보다는 "(사람 성격이) 과민한, 짜증 잘 내는"의 뜻이 있습니다. 재미있는 영어표현이네요.
 

 

 I got this Yale alumni event I have to go to.
 예일대 동창회에 가는 길입니다.
 alumni는 동창입니다. 발음은 ['얼럼니']가 아니라 ['얼럼나이']로 발음하셔야 합니다. 어원은 프랑스어인거 같네요.
 

 Oh, Yale is a prestigious school--
 예일대는 명문대학인데...
 영어로 명문대가 "prestigious school"입니다.
 인간 prestige가 "위신, 명망"이라는 뜻이고요. 형용사로 "위신있는"의 뜻도 있습니다.
 

 

 I'm fucking with you. Yeah. I know what Yale is.
 웃자고 제한 소리예요. 저도 예일대 알아요.
 단어 편측 그리하여 오해하시면 안돼요! I'm kidding with you, i'm joking with you랑 같은 뜻입니다. 농담한거란 거죠.
 

 

 You're exquisite.
 당신은 실 아름다워요.
 알아두면 과연 좋은 영어문장이네요.  exquisite 뜻이 형용사로 "매우 아름다운, 강렬한, 극상의"입니다. 발음은 [익스퀴짓]으로 하시면 되겠습니다.
 

 

 Bad back isn't hereditary.
 활리 빙처 좋은 건 유전 아니에요.
 hereditary 뜻은 형용사로 "유전적인"입니다. 무엇이 "유전이다/ 유전이 아니다"를 논할때 [인터넷 영화](https://cactusselect.tk/entertain/post-00004.html) 쓰면 좋은 영단어입니다.
 

 

 We should get him drunk and make him confess!
 술 그득 먹여서, 술술 불게 하죠!!
 make 수하 confess라고 하면 "자백하게 만들다"입니다. confess 뜻이 '자백하다'이고요. 주로 탐정수사물 또는 법정물 미드/영화에 이런즉 영어문장이 잔뜩 나오죠.
 참고로 confession이 고해성사입니다. 알아두시면 좋겠습니다.
 

 금대 good on paper를 비롯해서 각반 알아두면 유용한 영어문장과 표현들을 알아봤습니다. 영화 "겉보기엔 좋은 남자"는 6월 23일에 넷플릭스에서 볼 수 있습니다.
 

 아래에 예고편 영상과 더불어 예고편 풀자막을 옮겨뒀습니다. 보면서 추가로 영어공부 하셨으면 좋겠습니다.
 

 https://www.youtube.com/watch?v=TrahmVHDdeA

 

 I always respected that you weren't obsessed with marriage.
 It was okay that you hadn't reached all your goals by the age of 35.
 -Wow.
-Yeah?
-I'm 34.
-You look 35.
A salty 35.
 That compliment started great and just…
 For me, the story was not about love at first sight.
 You're a comedian, right?
 Andrea Singer. I've seen you before.
 -What do you do?
 -Hedge funds.
 I got this Yale alumni event I have to go to.
 What's a Yale?
 Oh, Yale is a prestigious school--
 I'm fucking with you. Yeah. I know what Yale is.
 He seemed nice, normal.
 Like an accountant who loves missionary.
 I want you to be my girlfriend.
You are so witty and sexy.
You're exquisite.
In that moment, I didn't want to be with anyone but him.
You don't really know anything about him, do you?
I mean, you know he bought a house months ago.
-So where is it?
-Beverly Hills.
-But where is it specifically?
-Beverly Hills.
You ever notice he always has the perfect answer for everything?
-Absolutely.
-Absolute ly.
Absolutely.
I'm taking Dennis to meet Cousin Brett.
-That's a big step.
-A big step!
-Andrea said you played at Yale.
-Show 'em how it's done.
-Oh my God!
-He's so old!
So embarrassing. My father has a bad back. I have a bad back.
Bad back isn't hereditary.
We both got in a car accident.
You both hurt your back in the same place?
We both hurt our backs in the same place.
Dennis is a cuttlefish.
"Lesser cuttlefish mate with females under false pretenses."
-So it's like catfishing, but worse.
-Cuttlefishing! I love that!
We should get him drunk and make him confess!
I got you, girl.
Let's ride!
Built quite the iron stomach during my Yale years.
Now that he's here, y'all could work it out.
He is unconscious!
Was that your plan?
Absolutely.
 

 2021.06.02 - [미드로 영어공부] - 넷플릭스 6월 신작 미드 추천! 스위트 투스 예고편으로 영어공부
 2021.05.31 - [영화로 영어공부] - [영어회화]영화, 스케이터걸 예고편 "상관없어, ~든지, 무조건" 영어로!!
 2021.05.28 - [영어 문법, 어휘, 회화/영어 어휘] - [영어 신조어]질문 그만해!! 질문충 영어로
 2021.05.27 - [영화로 영어공부] - 넷플릭스 실화바탕 가족영화, 아빠가 되는 한가운데 예고편으로 영어단어 공부
 

